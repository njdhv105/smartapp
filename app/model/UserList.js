Ext.define('Smartapp.model.UserList', {
    extend: 'Ext.data.Model',
    alias: 'model.userlist',
    idProperty: 'id',
    fields: [
        'name', 'description', 'jobTitle'
    ]
});