Ext.define('Smartapp.view.main.MainController', {

    extend: 'Ext.app.ViewController',

    alias: 'controller.main',
    /**
     * This is common function to reset the selected items and hide the buttons 
     */
    doResetSelectedItem: function () {
        this.getViewModel().set('isEditDelete', false);
        this.selectedRecord = null;
        this.getView().down('user-form').reset();
    },
    /**
     * onSaveButtonClick function will fire when save button clicked.
     * @param {Ext.button.Button}
     * @param {Ext.event.Event} e
     */
    onSaveButtonClick: function (button, e) {
        var values = button.up('user-form').getValues(),
            store = button.up('home-view').down('user-list').getStore(),
            record;
        //If id is present then need to update the data otherewise add as new entry
        if (values.id) {
            record = store.findRecord('id', values.id);
            delete values.id;
            record.set(values);
        } else {
            record = new Ext.create('Smartapp.model.UserList', values)
            store.add(record);
        }
        store.sync(); //save the data to the Web local Storage
        button.up('user-form').reset();
    },

    /**
     * onCancelButtonClick function will fire when cancel button clicked.
     * @param {Ext.button.Button} 
     * @param {Ext.event.Event} e
     */
    onCancelButtonClick: function (button, e) {
        button.up('user-form').reset();
    },

    /**
     * onUserSelect function will fire when user list item selected.
     * @param {Ext.view.View} 
     * @param {Ext.data.Model} The selected record.
     * @param {Number} The index within the store of the selected record.
     * @param {Object} The options object passed to Ext.util.Observable.addListener.
     */
    onUserSelect: function (dataview, record, index, eOpts) {
        this.getViewModel().set('isEditDelete', true);
        this.selectedRecord = record;
    },

    /**
     * onUserSelect function will fire when user list item selected.
     * @param {Ext.view.View} 
     * @param {Ext.data.Model} The selected record.
     * @param {Number} The index within the store of the selected record.
     * @param {Object} The options object passed to Ext.util.Observable.addListener.
     */
    onUserDeSelect: function (dataview, record, index, eOpts) {
        this.doResetSelectedItem();
    },

    /**
     * onEditButtonClick function will fire when edit button clicked.
     * @param {Ext.button.Button} 
     * @param {Ext.event.Event} e
     */
    onEditButtonClick: function (button, e) {
        //We cat set data using loadRecord or form.getForm().setValues()
        this.getView().down('user-form').loadRecord(this.selectedRecord);
        //this.getViewModel().set('formData', this.selectedRecord.data);
    },

    /**
     * onDeleteButtonClick function will fire when delete button clicked.
     * @param {Ext.button.Button} 
     * @param {Ext.event.Event} e
     */
    onDeleteButtonClick: function (button, e) {
        var me = this,
            store = button.up('home-view').down('user-list').getStore();

        Ext.MessageBox.show({
            title: 'Confirmation',
            msg: 'Are you sure you want to delete?',
            buttons: Ext.MessageBox.OKCANCEL,
            icon: Ext.MessageBox.WARNING,
            fn: function (btn) {
                if (btn == 'ok') {
                    store.remove(me.selectedRecord);
                    store.sync();
                    me.doResetSelectedItem();
                }
            }
        });
    }
});