Ext.define('Smartapp.store.UserList', {
    extend: 'Ext.data.Store',
    alias: 'store.userlist',
    model: 'Smartapp.model.UserList',
    autoLoad: true,
    proxy: {
        type: 'localstorage',
        id: 'SmartappProxyKey'
    }
});