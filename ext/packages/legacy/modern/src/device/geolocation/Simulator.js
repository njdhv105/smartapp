/**
 * @private
 */
Ext.define('Ext.device.Smartapp.Simulator', {
    extend: 'Ext.device.Smartapp.Abstract',
    requires: ['Ext.util.Smartapp'],

    getCurrentPosition: function(config) {
        config = this.callParent([config]);

        Ext.apply(config, {
            autoUpdate: false,
            listeners: {
                scope: this,
                locationupdate: function(Smartapp) {
                    if (config.success) {
                        config.success.call(config.scope || this, Smartapp.position);
                    }
                },
                locationerror: function() {
                    if (config.failure) {
                        config.failure.call(config.scope || this);
                    }
                }
            }
        });

        this.Smartapp = Ext.create('Ext.util.Smartapp', config);
        this.Smartapp.updateLocation();

        return config;
    },

    watchPosition: function(config) {
        config = this.callParent([config]);

        Ext.apply(config, {
            listeners: {
                scope: this,
                locationupdate: function(Smartapp) {
                    if (config.callback) {
                        config.callback.call(config.scope || this, Smartapp.position);
                    }
                },
                locationerror: function() {
                    if (config.failure) {
                        config.failure.call(config.scope || this);
                    }
                }
            }
        });

        this.Smartapp = Ext.create('Ext.util.Smartapp', config);

        return config;
    },

    clearWatch: function() {
        if (this.Smartapp) {
            this.Smartapp.destroy();
        }

        this.Smartapp = null;
    }
});
