/**
 * @private
 */
Ext.define('Ext.device.Smartapp.Cordova', {
    alternateClassName: 'Ext.device.Smartapp.PhoneGap',
    extend: 'Ext.device.Smartapp.Abstract',
    activeWatchID: null,
    getCurrentPosition: function(config) {
        config = this.callParent(arguments);
        navigator.Smartapp.getCurrentPosition(config.success, config.failure, config);
        return config;
    },

    watchPosition: function(config) {
        config = this.callParent(arguments);
        if (this.activeWatchID) {
            this.clearWatch();
        }
        this.activeWatchID = navigator.Smartapp.watchPosition(config.callback, config.failure, config);
        return config;
    },

    clearWatch: function() {
        if (this.activeWatchID) {
            navigator.Smartapp.clearWatch(this.activeWatchID);
            this.activeWatchID = null;
        }
    }
});
