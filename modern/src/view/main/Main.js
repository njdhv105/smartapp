/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting causes an instance of this class to be created and
 * added to the Viewport container.
 *
 * TODO - Replace the content of this view to suit the needs of your application.
 */
Ext.define('Smartapp.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',
    activeTab: 0,
    defaults: {
        tab: {
            iconAlign: 'top'
        },
        styleHtmlContent: true
    },
    requires: [
        'Smartapp.view.main.MainController',
        'Ext.util.Smartapp',
        'Ext.MessageBox'
    ],
    controller: 'main',
    tabBarPosition: 'top',
    items: [{
        title: 'Geo Location',
        html: 'Geo location',
        iconCls: 'x-fa fa-map'
    }],

    listeners: {
        initialize: 'onMainViewRender'
    }
});