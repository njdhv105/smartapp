Ext.define('Smartapp.view.main.Main', {
    extend: 'Ext.panel.Panel',
    xtype: 'app-main',
    layout: 'card',
    requires: ['Smartapp.view.*'],
    items: [{
        xtype: 'home-view'
    }]
});