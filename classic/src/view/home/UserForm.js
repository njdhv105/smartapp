Ext.define('Smartapp.view.home.UserForm', {
    extend: 'Ext.form.Panel',
    xtype: 'user-form',
    defaults: {
        xtype: 'textfield',
        labelAlign: 'top',
        width: '100%',
        allowBlank: false,
        labelCls: 'user-label',
        margin: '20 0'
    },
    title: 'Add/Update',
    scrollable: true,
    cls: 'user-form',
    padding: '20 10',
    bodyPadding: 20,
    items: [{
        xtype: 'hiddenfield',
        name: 'id',
        bind: '{id}'
    }, {
        fieldLabel: 'Name',
        name: 'name',
        emptyText: 'Name',
        bind: '{name}'
    }, {
        xtype: 'textareafield',
        fieldLabel: 'Description',
        name: 'description',
        emptyText: 'Description',
        bind: '{description}'
    }, {
        fieldLabel: 'Job Title',
        emptyText: 'Job Title',
        name: 'jobTitle',
        bind: '{jobTitle}'
    }],
    bbar: ['->', {
        text: 'Cancel',
        handler: 'onCancelButtonClick'
    }, {
        text: 'Save',
        handler: 'onSaveButtonClick',
        formBind: true
    }]
});