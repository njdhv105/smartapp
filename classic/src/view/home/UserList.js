Ext.define('Smartapp.view.home.UserList', {
    extend: 'Ext.view.View',
    xtype: 'user-list',
    tpl: new Ext.XTemplate(
        `<tpl for=".">
            <div class="user-container">
                <p> <small>Name</small> : {name}</p>
                <p class="description"> <small>Description</small>  : {description}</p>
                <p> <small>Job </small> : {jobTitle}</p>
            </div>
        </tpl>`
    ),
    itemSelector: 'div.user-container',
    store: Ext.create('Smartapp.store.UserList'),
    emptyText: '<div class="empty-text">No record available </div>'
});