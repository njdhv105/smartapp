Ext.define('Smartapp.view.home.Home', {
    extend: 'Ext.container.Container',
    xtype: 'home-view',
    layout: 'hbox',
    cls: 'x-home-view',
    controller: 'main',
    padding: '10 20',
    viewModel: {
        data: {
            isEditDelete: false,
            formData: {}
        }
    },
    defaults: {
        height: '100%'
    },
    items: [{
        xtype: 'panel',
        flex: 0.70,
        tbar: ['->', {
            text: 'Edit',
            bind: {
                disabled: '{!isEditDelete}'
            },
            handler: 'onEditButtonClick'
        }, {
            text: 'Delete',
            bind: {
                disabled: '{!isEditDelete}'
            },
            handler: 'onDeleteButtonClick'
        }],
        items: [{
            xtype: 'user-list',
            height: '100%',
            scrollable: 'vertical',
            listeners: {
                select: 'onUserSelect',
                deselect: 'onUserDeSelect'
            },
            cls: 'user-list'
        }]
    }, {
        minWidth: 20,
        height: '100%'
    }, {
        flex: 0.30,
        xtype: 'user-form'
    }]
});